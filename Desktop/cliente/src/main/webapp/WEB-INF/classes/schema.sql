CREATE TABLE IF NOT EXISTS tipo_pessoa_tb
(
	id_tipo_pessoa integer not null IDENTITY,
	dc_tipo_pessoa varchar(255) not null,
	primary key(id_tipo_pessoa)

);

CREATE TABLE IF NOT EXISTS cliente_tb
(
   id_cliente integer not null IDENTITY,
   nome varchar(50) not null,
   id_tipo_pessoa integer not null,
   ic_ativo boolean not null default 1,
   primary key(id_cliente),
   FOREIGN KEY(id_tipo_pessoa)
     REFERENCES tipo_pessoa_tb(id_tipo_pessoa)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION
);