package com.cliente.enums;

public enum TipoPessoaEnum {
	FISICA("Física", 1), JURIDICA("Jurídica", 2);

	private final String chave;
	private final Integer valor;

	TipoPessoaEnum(String chave, Integer valor) {
		this.chave = chave;
		this.valor = valor;
	}

	public String getChave() {
		return chave;
	}

	public Integer getValor() {
		return valor;
	}

}
