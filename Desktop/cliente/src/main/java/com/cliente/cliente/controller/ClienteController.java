package com.cliente.cliente.controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cliente.cliente.dao.RepositorioCliente;
import com.cliente.cliente.to.Cliente;
import com.cliente.cliente.to.PessoaFisica;
import com.cliente.cliente.to.PessoaJuridica;
import com.cliente.enums.TipoPessoaEnum;

@Scope(value="session")
@Component(value = "clienteController")
@ELBeanName(value = "clienteController")
@Join(path = "/cliente", to = "/cliente/cadastro-cliente.jsf")
public class ClienteController {

	@Autowired
	private RepositorioCliente repositorio;
	
	private Cliente cliente;
	
	private boolean exibirCamposPF;
	private boolean exibirCamposPJ;

	public ClienteController() {
		exibirCamposPF=false;
		exibirCamposPJ=false;
		cliente = new Cliente();
	}
	
	
	public TipoPessoaEnum[] getTipoPessoa() {
		return TipoPessoaEnum.values();
	}

	public String salvar() {
		
		repositorio.save(cliente);
		cliente = new Cliente();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cliente Cadastrado", null);
		FacesContext.getCurrentInstance().addMessage("Teste teste", message);
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/cliente/cliente-lista.xhtml?faces-redirect=true";
	}

	public Cliente getCliente() {
		return cliente;
	}


	public void renderizar() {  
		switch (cliente.getPessoa().getIdTipoPessoa()) {
		case 1:
			exibirCamposPJ = false;  
			exibirCamposPF = true;
			cliente = new Cliente(new PessoaFisica());
			break;
			
		case 2:
			exibirCamposPJ = true;  
			exibirCamposPF = false;
			cliente = new Cliente(new PessoaJuridica());
			break;

		default:
			exibirCamposPJ = false; 
			exibirCamposPF = false;
			cliente = new Cliente();
			break;
		}
        
    }  


	
	
	public boolean isExibirCamposPJ() {
		return exibirCamposPJ;
	}


	public void setExibirCamposPJ(boolean exibirCamposPJ) {
		this.exibirCamposPJ = exibirCamposPJ;
	}


	public boolean isExibirCamposPF() {
		return exibirCamposPF;
	}


	public void setExibirCamposPF(boolean exibirCamposPF) {
		this.exibirCamposPF = exibirCamposPF;
	}

	


	
	

}
