package com.cliente.cliente.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.cliente.cliente.dao.RepositorioCliente;
import com.cliente.cliente.to.Cliente;
import com.cliente.enums.TipoPessoaEnum;

@Scope(value="session")
@Component(value = "listaClientes")
@ELBeanName(value = "listaClientes")
@Join(path = "/", to = "/cliente/cliente-lista.jsf")
public class ListaClienteController {

	@Autowired
	private RepositorioCliente repositorio;

	private List<Cliente> clientes;

	private Cliente clienteSelecionado;



	public ListaClienteController() {
		clienteSelecionado = new Cliente();
	}

	@Deferred
	@RequestAction
	@IgnorePostback
	public void loadData() {
		clientes = repositorio.findAll();
		return;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public String deletarCliente() {

		repositorio.deleteById(clienteSelecionado.getIdCliente());
		loadData();
		getClientes();
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cliente Excluído, pressione F5", null);
		FacesContext.getCurrentInstance().addMessage("Cliente Excluído", message);
		return null;
	}

	public Cliente getClienteSelecionado() {
		return clienteSelecionado;
	}

	public void setClienteSelecionado(Cliente clienteSelecionado) {
		this.clienteSelecionado = clienteSelecionado;
	}

	// Metodos para edição na tabela
	public void onRowEdit(RowEditEvent event) {
		Cliente clienteEditado = (Cliente) event.getObject();
		repositorio.save(clienteEditado);
		FacesMessage msg = new FacesMessage("Edição de cliente ", "O cliente "+clienteEditado.getPessoa().getNome()+" foi editado com sucesso!");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onRowCancel(RowEditEvent event) {
		Cliente clienteEditado = (Cliente) event.getObject();
		FacesMessage msg = new FacesMessage("Edição Cancelada",  "O cliente "+clienteEditado.getPessoa().getNome()+" não editado!");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onCellEdit(CellEditEvent event) {
		Object oldValue = event.getOldValue();
		Object newValue = event.getNewValue();

		if (newValue != null && !newValue.equals(oldValue)) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Célula Editada",
					"Antes: " + oldValue + ", Agora:" + newValue);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}
	
	

}
