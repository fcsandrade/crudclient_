package com.cliente.servicos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.cliente.cliente.dao.RepositorioCliente;
import com.cliente.cliente.to.Cliente;
import com.cliente.cliente.to.PessoaFisica;

@RestController
public class ClienteService {
	
	@Autowired
	private RepositorioCliente repositorio;
	
	@GetMapping("/validacao/{cpf}")
	public PessoaFisica existeCPF(@PathVariable String cpf) {
		
		PessoaFisica pf = repositorio.buscaPorCPF(cpf);
		return pf;
	}
	
	@GetMapping("/validacao")
	public List<Cliente> retrieveAllStudents() {
		return repositorio.findAll();
	}

}
